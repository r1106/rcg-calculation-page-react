import { INIT_CALCULATION_VALUES } from "../../types/consts/init-calculation-values.const";
import { CalculationFieldNamesEnum } from "../../types/enums/calculation-field-names.enum";
import { ICalculationValues } from "../../types/interfaces/calculation-values.interface";
import UserInput from "../user-input/UserInput";
import "./UserInputContainer.scss";

function UserInputContainer({
  calculationValues,
  collectCalculationValues: collectData,
}: {
  calculationValues: ICalculationValues;
  collectCalculationValues: (data: ICalculationValues) => void;
}): JSX.Element {
  function collectUserInputs(data: ICalculationValues): void {
    collectData({ ...data });
  }

  return (
    <section id="user-input">
      <div className="input-group">
        <UserInput
          label={CalculationFieldNamesEnum.InitialInvestment}
          initValue={INIT_CALCULATION_VALUES.initialInvestment}
          onChange={(event) =>
            collectUserInputs({
              ...calculationValues,
              initialInvestment: Number(event.target.value),
            })
          }
        ></UserInput>
        <UserInput
          label={CalculationFieldNamesEnum.AnnualInvestment}
          initValue={INIT_CALCULATION_VALUES.annualInvestment}
          onChange={(event) =>
            collectUserInputs({
              ...calculationValues,
              annualInvestment: Number(event.target.value),
            })
          }
        ></UserInput>
      </div>
      <div className="input-group">
        <UserInput
          label={CalculationFieldNamesEnum.ExpectedReturn}
          initValue={INIT_CALCULATION_VALUES.expectedReturn}
          onChange={(event) =>
            collectUserInputs({
              ...calculationValues,
              expectedReturn: Number(event.target.value),
            })
          }
        ></UserInput>
        <UserInput
          label={CalculationFieldNamesEnum.Duration}
          initValue={INIT_CALCULATION_VALUES.duration}
          onChange={(event) =>
            collectUserInputs({
              ...calculationValues,
              duration: Number(event.target.value),
            })
          }
        ></UserInput>
      </div>
    </section>
  );
}

export default UserInputContainer;
