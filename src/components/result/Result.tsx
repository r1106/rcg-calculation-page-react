import { CalculationTheadNamesEnum } from "../../types/enums/calculation-thead-names.enum";
import { ICalculationResultValues } from "../../types/interfaces/calculation-result-values.interface";
import { ICalculationValues } from "../../types/interfaces/calculation-values.interface";
import { calculateInvestmentResults, formatter } from "../../utils/investment";
import "./Result.scss";

function Result({
  calculationValues,
}: {
  calculationValues: ICalculationValues;
}): JSX.Element {
  const calculatedResultValues: ICalculationResultValues[] =
    calculateInvestmentResults(calculationValues);
  const initialInvestment =
    calculatedResultValues[0].valueEndOfYear -
    calculatedResultValues[0].interest -
    calculatedResultValues[0].annualInvestment;

  return (
    <table id="result">
      <thead>
        <tr>
          <th>{CalculationTheadNamesEnum.Year}</th>
          <th>{CalculationTheadNamesEnum.InvestmentValue}</th>
          <th>{CalculationTheadNamesEnum.InterestYear}</th>
          <th>{CalculationTheadNamesEnum.TotalInterest}</th>
          <th>{CalculationTheadNamesEnum.InvestmentCapital}</th>
        </tr>
      </thead>
      <tbody>
        {calculatedResultValues.map((values, index) => {
          const totalInterest =
            values.valueEndOfYear -
            values.annualInvestment * values.year -
            initialInvestment;

          const totalAmountInvestement = values.valueEndOfYear - totalInterest;

          return (
            <tr key={index}>
              <td className="center">{values.year}</td>
              <td className="center">
                {formatter.format(values.valueEndOfYear)}
              </td>
              <td className="center">{formatter.format(values.interest)}</td>
              <td className="center">{formatter.format(totalInterest)}</td>
              <td className="center">
                {formatter.format(totalAmountInvestement)}
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default Result;
