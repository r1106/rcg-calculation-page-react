import logo from "../../assets/investment-calculator-logo.png";
import "./Header.scss";

function Header(): JSX.Element {
  return (
    <header id="header">
      <img src={logo} alt="logo"></img>
      <h1>Calculation Page</h1>
    </header>
  );
}

export default Header;
