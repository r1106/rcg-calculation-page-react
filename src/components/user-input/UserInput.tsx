import { ChangeEventHandler } from "react";

function UserInput({
  label,
  initValue,
  onChange,
}: {
  label: string;
  initValue: number;
  onChange: ChangeEventHandler<HTMLInputElement>;
}) {
  return (
    <p>
      <label>{label}</label>
      <input
        onChange={onChange}
        defaultValue={initValue}
        type="number"
        min={1}
      />
    </p>
  );
}

export default UserInput;
