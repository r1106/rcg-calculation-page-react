import { Dispatch, SetStateAction, useState } from "react";
import Header from "./components/header/Header";
import Result from "./components/result/Result";
import UserInputContainer from "./components/user-input-container/UserInputContainer";
import { INIT_CALCULATION_VALUES } from "./types/consts/init-calculation-values.const";
import { ICalculationValues } from "./types/interfaces/calculation-values.interface";

function App(): JSX.Element {
  const [calculationValues, setCalculationValues]: [
    ICalculationValues,
    Dispatch<SetStateAction<ICalculationValues>>
  ] = useState<ICalculationValues>(INIT_CALCULATION_VALUES);

  function collectCalculationValues(data: ICalculationValues): void {
    setCalculationValues(() => data);
  }

  return (
    <>
      <Header></Header>
      <UserInputContainer
        calculationValues={calculationValues}
        collectCalculationValues={collectCalculationValues}
      ></UserInputContainer>
      {calculationValues.duration > 0 ? (
        <Result calculationValues={calculationValues}></Result>
      ) : (
        <p id="not-valid">Please insert Valid Numbers</p>
      )}
    </>
  );
}

export default App;
