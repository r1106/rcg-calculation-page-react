export enum CalculationTheadNamesEnum {
  Year = "Year",
  InvestmentValue = "Investment Value",
  InterestYear = "Interest (Year)",
  TotalInterest = "Total Interest",
  InvestmentCapital = "Investment Capital",
}
