export enum CalculationFieldNamesEnum {
  InitialInvestment = "Initial Investment",
  AnnualInvestment = "Annual Investment",
  ExpectedReturn = "Expected Return",
  Duration = "Duration",
}
