export const INIT_CALCULATION_VALUES = {
  initialInvestment: 12,
  annualInvestment: 2300,
  expectedReturn: 2343,
  duration: 2,
};
