export interface ICalculationValues {
  initialInvestment: number;
  annualInvestment: number;
  expectedReturn: number;
  duration: number;
}
