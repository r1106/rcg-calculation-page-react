export interface ICalculationResultValues {
  year: number;
  interest: number;
  valueEndOfYear: number;
  annualInvestment: number;
}
